﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Shared
{
	public enum CarType
	{
		Passenger,
		Truck,
		Bus,
		Motorcycle
	}
}
