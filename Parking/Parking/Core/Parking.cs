﻿using System;
using System.Collections.Generic;
using System.Threading;
using Parking.Shared;

namespace Parking.Core
{
	public sealed class Parking : IDisposable
	{
		private static readonly Lazy<Parking> lazy = new Lazy<Parking>(() => new Parking());
		public static Parking Instanse { get { return lazy.Value; } }


		private List<Car> cars;
		private Timer carTimer;

		private List<Transaction> transactions;

		private List<Transaction> transactionsForLogging;
		private Timer logTimer;

		private Timer deleteOldTransactionsTimer;

		public double Balance { get; private set; }

		private Parking()
		{
			cars = new List<Car>();
			transactions = new List<Transaction>();
			transactionsForLogging = new List<Transaction>();
			carTimer = new Timer(Payment, null, Settings._timeout * 1000, Settings._timeout * 1000);
			logTimer = new Timer(Log, null, Settings._logTimeout * 1000, Settings._logTimeout * 1000);
			deleteOldTransactionsTimer = new Timer(DeleteOldTransaction, null, (Settings._logTimeout + 1) * 1000, 1000);
		}



		public bool AddCar(CarType type, double balance, out string Guid)
		{
			try
			{
				if (Settings._parkingSpace > cars.Count)
				{
					var tempCar = new Car(type);
					tempCar.AddMoney(balance);
					Guid = tempCar.ID;
					cars.Add(tempCar);
					return true;
				}
				Guid = null;
				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}


		public bool RemoveCar(string Guid)
		{
			try
			{
				var carForRemove = cars.Find(car => car.ID == Guid);
				if (carForRemove.Balance > 0)
				{
					cars.Remove(carForRemove);
					return true;
				}
				else
					return false;
			}
			catch (Exception)
			{
				throw;
			}
		}


		public void AddBalance(string Guid, double money)
		{
			try
			{
				var carForAddMoney = cars.Find(car => car.ID == Guid);
				if (carForAddMoney.Fine > 0)
				{
					Transaction transaction;
					carForAddMoney.PayFine(money, out transaction);
					Balance += transaction.Tax;
					transactions.Add(transaction);
				}
				else
					carForAddMoney.AddMoney(money);

			}
			catch (Exception)
			{
				throw;
			}
		}

		private double CalculateTax(Car car)
		{
			try
			{
				if (car.Balance < 0 || car.Balance < Settings._bills[car.Type.ToString()])
					return Settings._fine * Settings._bills[car.Type.ToString()];
				return Settings._bills[car.Type.ToString()];
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		#region ShowMethods

		public List<Transaction> ShowTransactions()
		{
			return transactions;
		}

		public int TotalPlaces()
		{
			return Settings._parkingSpace;
		}

		public int FreePlaces()
		{
			return Settings._parkingSpace - cars.Count;
		}

		public int OccupiedPlaces()
		{
			return cars.Count;
		}

		public double ShowIncome(bool minute)
		{
			if (minute)
				return MinuteIncomeCalculate();
			else
				return Balance;
		}

		private double MinuteIncomeCalculate()
		{
			double income = 0;
			foreach (var transaction in transactions)
			{
				income += transaction.Tax;
			}
			return income;
		}

		public string ShowLog()
		{
			return Logger.ReadLog(Settings._logPath);
		}

		public string ShowCar(string Guid)
		{
			try
			{
				var carForDisplay = cars.Find(car => car.ID == Guid);
				return carForDisplay.ToString();
			}
			catch (Exception)
			{
				throw;
			}
		}

		#endregion ShowMethods


		private void Payment(object obj)
		{
			try
			{
				foreach (var car in cars)
				{
					var tax = CalculateTax(car);
					if (tax > car.Balance)
					{
						car.Fine += tax;
					}
					else
					{
						Transaction transaction;
						car.Pay(CalculateTax(car), out transaction);
						Balance += transaction.Tax;
						transactions.Add(transaction);
					}
				}
			}
			catch (Exception ex)
			{
				Logger.WriteException(ex.Message, "Parking.log");
			}
		}

		private void Log(object obj)
		{
			double balance = 0;
			foreach (var t in transactions)
				balance += t.Tax;
			Logger.WriteLog(balance, Settings._logPath);
		}

		private void DeleteOldTransaction(object obj)
		{
			try
			{
				for (int counter = 0; counter < transactions.Count; counter++)
				{
					if (transactions[counter].TransactionTime < DateTime.Now.AddSeconds(-Settings._logTimeout))
						transactions.RemoveAt(counter);
					else
						break;
				}
			}
			catch (Exception ex)
			{
				Logger.WriteException(ex.Message, "Parking.log");
			}
		}

		public void Dispose()
		{
			carTimer.Dispose();
			deleteOldTransactionsTimer.Dispose();
			logTimer.Dispose();
		}
	}

}

